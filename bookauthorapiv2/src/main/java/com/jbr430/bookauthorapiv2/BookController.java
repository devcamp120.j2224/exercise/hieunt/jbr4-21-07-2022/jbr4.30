package com.jbr430.bookauthorapiv2;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class BookController {
    @GetMapping("/books")
    public static ArrayList<Book> getBookList() {
        Author author1 = new Author("Lan", "lan@gmail.com", 'f');
        Author author2 = new Author("Hoa", "hoa@gmail.com", 'f');
        Author author3 = new Author("Nam", "nam@gmail.com", 'm');
        Author author4 = new Author("Trung", "trung@gmail.com", 'm');
        Author author5 = new Author("Hue", "hue@gmail.com", 'f');
        Author author6 = new Author("Minh", "minh@gmail.com", 'm');
        System.out.println("author1 la: " + author1.toString());
        System.out.println("author2 la: " + author2.toString());
        System.out.println("author3 la: " + author3.toString());
        System.out.println("author4 la: " + author4.toString());
        System.out.println("author5 la: " + author5.toString());
        System.out.println("author6 la: " + author6.toString());
        ArrayList<Author> authorlist1 = new ArrayList<Author>();
        authorlist1.add(author1);
        authorlist1.add(author2);
        ArrayList<Author> authorlist2 = new ArrayList<Author>();
        authorlist2.add(author3);
        authorlist2.add(author4);
        ArrayList<Author> authorlist3 = new ArrayList<Author>();
        authorlist3.add(author5);
        authorlist3.add(author6);
        Book book1 = new Book("Harry Potter", authorlist1, 90000, 7);
        Book book2 = new Book("Twilight", authorlist2, 110000, 9);
        Book book3 = new Book("Animal Farm", authorlist3, 130000, 6);
        System.out.println("book1 la: " + book1.toString());
        System.out.println("book2 la: " + book2.toString());
        System.out.println("book3 la: " + book3.toString());
        ArrayList<Book> bookList = new ArrayList<Book>();
        bookList.add(book1);
        bookList.add(book2);
        bookList.add(book3);
        return bookList;
    } 
}
