package com.jbr430.bookauthorapiv2;

import java.util.ArrayList;

public class Book {
    String name;
    ArrayList<Author> authors;
    double price;
    int qty = 0;
    public Book(String name, ArrayList<Author> authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }
    public Book(String name, ArrayList<Author> authors, double price, int qty) {
        this(name, authors, price);
        this.qty = qty;
    }
    public String getName() {
        return name;
    }
    public ArrayList<Author> getAuthors() {
        return authors;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public int getQty() {
        return qty;
    }
    public void setQty(int qty) {
        this.qty = qty;
    }
    @Override
    public String toString() {
        return "Book[name=" + name
        + ", authors=" + authors.toString()
        + ", price=" + price
        + ", qty=" + qty + "]";
    }
    public String getAuthorNames() {
        String result = "";
        if(authors.size() > 0) {
            for (Author bAuthor : authors) {
                result += bAuthor.getName() + ",";
            }
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }
}
